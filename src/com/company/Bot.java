package com.company;

/**
 * Created by jakubtustanowski on 05/12/2016.
 */
public abstract class Bot {

    private Difficulty difficulty;
    private String playerName;
    private GameType gameType;

    protected abstract Move nextMove(GameState state);

    public Bot(Difficulty difficulty, String playerName)
    {
        this.difficulty = difficulty;
        this.playerName = playerName;
    }

    protected PlayerState getMyStateFromGameState(GameState gameState) {
        for (PlayerState playerState : gameState.getPlayerStates()) {
            if (playerState.getPlayerId() == this.playerName) {
                return playerState;
            }
        }
        return null;
    }

    public Difficulty getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(Difficulty difficulty) {
        this.difficulty = difficulty;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public GameType getGameType() {
        return gameType;
    }

    public void setGameType(GameType gameType) {
        this.gameType = gameType;
    }
}
