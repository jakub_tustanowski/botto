package com.company;

/**
 * Created by jakubtustanowski on 05/12/2016.
 */
public class BotFactory {

    private static long numberOfBots;

    protected BotFactory()
    {
        numberOfBots=0;
    }

    public static Bot createBot(GameType gameType, Difficulty difficulty, String name)
    {
        numberOfBots++;
        if(gameType == GameType.NPlus)
        {
            return new NPlusBot(difficulty, name);
        }

        if(gameType == GameType.NStar)
        {
            return new NStarBot(difficulty, name);
        }

        if(gameType == GameType.Poker)
        {
            return new PokerBot(difficulty, name);
        }
        return null;
    }
}
