package com.company;

/**
 * Created by jakubtustanowski on 05/12/2016.
 */
public enum Difficulty {
    Easy, Hard
}
