package com.company;

import java.util.List;

/**
 * Created by jakubtustanowski on 06/12/2016.
 */
public class GameState {

    private List<PlayerState> playerStates;
    private int maxPoints;

    public List<PlayerState> getPlayerStates() {
        return playerStates;
    }

    public void setPlayerStates(List<PlayerState> playerStates) {
        this.playerStates = playerStates;
    }

    public int getMaxPoints() {
        return maxPoints;
    }

    public void setMaxPoints(int maxPoints) {
        this.maxPoints = maxPoints;
    }
}
