package com.company;

import java.math.BigInteger;

import static java.lang.Math.pow;

/**
 * Created by jakubtustanowski on 07/12/2016.
 */
public class Helper {

    protected static int nextDieValue(int current)
    {
        return (current+1)%6;
    }

    protected static int getRerollHas(boolean[] isRerolled) // get binary output and turn into decimal
    {
        int val = 0;
        for(int i=0;i<5;i++)
        {
            if(isRerolled[i])
            {
                val+= pow(2,i);
            }
        }
        return val;
    }

    protected static int setRerollHash(int currentHash, int position, boolean value)
    {
        if(BigInteger.valueOf(currentHash).testBit(position))
        {
            if(value)
                return currentHash;
            else
                return currentHash - (int)pow(2,position);
        }
        else
        {
            if(value)
                return currentHash + (int)pow(2,position);
            else
                return currentHash;
        }
    }

    protected static int findMaxHash(int[] goodRerolls, int[] allRerolls)
    {
        double max = 0.0;
        int maxHash = 0;
        for(int i=0;i<allRerolls.length;i++)
        {
            if(((double)(goodRerolls[i])) / allRerolls[i]>max)
            {
                maxHash = i;
                max = ((double)(goodRerolls[i])) / allRerolls[i];
            }
        }
        return maxHash;
    }

    protected static Move hashToMove(int hash, String playerName)
    {
        Boolean[] bools = new Boolean[5];
        BigInteger big = BigInteger.valueOf(hash);
        for(int i=0;i<5;i++)
        {
            bools[i] = big.testBit(i);
        }

        return new Move(bools, playerName);
    }
}

