package com.company;

import java.util.concurrent.TimeUnit;

/**
 * Created by jakubtustanowski on 06/12/2016.
 */
public interface Logic {

    public static boolean isFirstBetter(int[] firstFive, int[] secondFive, GameType type)
    {
        try {
            TimeUnit.MILLISECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return countRepeats(firstFive) > countRepeats(secondFive);
    };
    public PlayerState playerWithBestHand(GameState state);

    public static int countRepeats(int[] hand)
    {
        int max=0;
        int current;
        for(int i=1;i<=6;i++)
        {
            current=0;
            for(int j=0;j<5;j++)
            {
                if(hand[j]==i)
                    current++;
            }
            if(current>max)
                max=current;
        }
        return max;
    }
}
