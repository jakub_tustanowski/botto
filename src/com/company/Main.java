package com.company;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        BotFactory factory = new BotFactory();
        long lStartTime;
        long lEndTime;
        long output;



        /*            --   Poker Test  ---

        /* -----------------------------------------*/

        System.out.println("POKER TEST");

        Bot bot = BotFactory.createBot(GameType.Poker, Difficulty.Easy, "bot3");
        GameState gameState = new GameState();
        gameState.setMaxPoints(5);
        List<PlayerState> playerStates = new ArrayList<>();
        PlayerState state = new PlayerState();
        state.setDice( new int[]{5,4,3,2,1});
        state.setPlayerId("bot3");
        playerStates.add(state);
        gameState.setPlayerStates(playerStates);

        lStartTime = System.nanoTime();
        Move move = bot.nextMove(gameState);
        lEndTime = System.nanoTime();

        for(int i=0;i<move.toReroll.length;i++)
            System.out.print(move.toReroll[i] + " ");

        output = lEndTime - lStartTime;

        System.out.println("\nElapsed time in miliseconds: " + output / 1000000);

        /* -----------------------------------------*/
    }
}
