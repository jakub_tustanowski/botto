package com.company;

/**
 * Created by jakubtustanowski on 05/12/2016.
 */
public class NPlusBot extends Bot {

    @Override
    public Move nextMove(GameState state) {
        return null;
    }

    public NPlusBot(Difficulty difficulty, String playerName) {
        super(difficulty, playerName);
        setGameType(GameType.NPlus);
    }
}
