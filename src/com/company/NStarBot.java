package com.company;

/**
 * Created by jakubtustanowski on 05/12/2016.
 */
public class NStarBot extends Bot {

    @Override
    public Move nextMove(GameState state) {
        return null;
    }

    public NStarBot(Difficulty difficulty, String playerName) {
        super(difficulty, playerName);
        setGameType(GameType.NStar);
    }
}
