package com.company;

/**
 * Created by jakubtustanowski on 06/12/2016.
 */
public class PlayerState {
    private String playerName;
    private int points;
    private int[] dice = new int[5];

    public String getPlayerId() {
        return playerName;
    }

    public void setPlayerId(String playerName) {
        this.playerName = playerName;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public int[] getDice() {
        return dice;
    }

    public void setDice(int[] dice) {
        this.dice = dice;
    }
}
