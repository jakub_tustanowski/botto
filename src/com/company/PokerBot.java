package com.company;

import java.util.Arrays;

import static com.company.Helper.*;

/**
 * Created by jakubtustanowski on 05/12/2016.
 */
public class PokerBot extends Bot {

    @Override
    protected Move nextMove(GameState state) {
        PlayerState playerState = getMyStateFromGameState(state);
        int [] playerDice = playerState.getDice();
        int [] tempDice = Arrays.copyOf(playerDice, playerDice.length);
        int [] goodRerolls = new int [32];
        int [] allRerolls = new int [32];

        checkAndGoDeeper(0, tempDice, playerDice, goodRerolls, allRerolls, 0);

        int finalHash = findMaxHash(goodRerolls,allRerolls);

        return hashToMove(finalHash, getPlayerName());

    }

    protected PokerBot(Difficulty difficulty, String playerName) {
        super(difficulty, playerName);
        setGameType(GameType.Poker);
    }

    private void checkAndGoDeeper(int current, int[] tempDice, int[] playerDice, int[] goodRerolls, int[] allRerolls, int rerollHash)
    {
        int myRerollHash = rerollHash;

        for(int i=0;i<6;i++)
        {
            tempDice[current] = nextDieValue(tempDice[current]);
            if(i==5)
            {
                myRerollHash = setRerollHash(rerollHash, current, false);
            }
            else
            {
                myRerollHash = setRerollHash(rerollHash, current, true);
            }

            allRerolls[myRerollHash]++;
            if (Logic.isFirstBetter(tempDice, playerDice, getGameType())) {
                goodRerolls[myRerollHash]++;
            }
            if(current<4)
                checkAndGoDeeper(current+1, tempDice, playerDice, goodRerolls, allRerolls, myRerollHash);
        }
    }
}
